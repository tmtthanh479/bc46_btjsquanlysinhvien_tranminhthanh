function nhanvien(taiKhoan, ten, email, matKhau,ngayLam, chucVu, luong, gioLam,) {
  // nhớ đúng thứ tự nhé
  this.taiKhoan = taiKhoan;
  this.ten = ten;
  this.email = email;
  this.matKhau = matKhau;
  this.ngayLam= ngayLam;
  this.chucVu = chucVu;
  this.luong = luong;
  this.gioLam = gioLam;
  this.tinhLuong = function () {
    if (this.chucVu === "Sếp") {
      return this.luong * 3;
    } else if (this.chucVu === "Trưởng phòng") {
        return this.luong * 2;
    } else if (this.chucVu === "Nhân viên") {
        return  this.luong * 1;
    }
  };
  this.xepLoai = function() {
    if (this.gioLam  >= 192) {
      return `<td>"Nhân viên xuất sắc"</td>`
    } else if (this.gioLam  >= 176) {
      return `<td>"Nhân viên giỏi"</td>`
    } else if (this.gioLam  >= 160) {
      return `<td>"Nhân viên khá"</td>`
    } else {
      return `<td>"Nhân viên trung bình"</td>`
    }
  };
}
