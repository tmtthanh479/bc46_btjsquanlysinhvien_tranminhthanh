function kiemTraTaiKhoan() {
  var taiKhoan = document.getElementById("tknv").value;
  if ( taiKhoan === '') {
    document.getElementById("tbTKNV").innerText = "Tài khoản không được để trống";
    document.getElementById("tbTKNV").style.display = "inline";
    return false;
  } else if (!/^\w{4,6}$/.test(taiKhoan)) {
    document.getElementById("tbTKNV").innerText = "Tài khoản phải có từ 4 đến 6 ký tự";
    document.getElementById("tbTKNV").style.display = "inline";
    return false;
  } else {
    document.getElementById("tbTKNV").innerText = "";
    document.getElementById("tbTKNV").style.display = "none";
    return true;
  }
}


function kiemTraTen(ten) {
  if (ten === '') {
    document.getElementById("tbTen").innerText = "Tên nhân viên không được để trống";
    document.getElementById("tbTen").style.display = "inline";
    return false;
  } else if (!/^[\p{L} ]+$/u.test(ten)) {
    document.getElementById("tbTen").innerText = "Tên nhân viên chỉ được chứa các ký tự chữ";
    document.getElementById("tbTen").style.display = "inline";
    return false;
  } else {
    document.getElementById("tbTen").innerText = "";
    document.getElementById("tbTen").style.display = "none";
    return true;
  }
}


function kiemTraEmail(email) {
  var emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (email === '') {
    document.getElementById("tbEmail").innerText = "Email không được để trống";
    document.getElementById("tbEmail").style.display = "inline";
    return false;
  } else if (!emailPattern.test(email)) {
    document.getElementById("tbEmail").innerText = "Email không hợp lệ";
    document.getElementById("tbEmail").style.display = "inline";
    return false;
  } else {
    document.getElementById("tbEmail").innerText = "";
    document.getElementById("tbEmail").style.display = "none";
    return true;
  }
}

function kiemTraMatKhau(matKhau) {
  if (matKhau === '') {
    document.getElementById("tbMatKhau").innerText = "Mật khẩu không được để trống";
    document.getElementById("tbMatKhau").style.display = "inline";
    return false;
  } else if (matKhau.length < 6 || matKhau.length > 10) {
    document.getElementById("tbMatKhau").innerText = "Mật khẩu phải từ 6 đến 10 ký tự";
    document.getElementById("tbMatKhau").style.display = "inline";
    return false;
  } else if (!/[0-9]/.test(matKhau) || !/[A-Z]/.test(matKhau) || !/[!@#$%^&*]/.test(matKhau)) {
    document.getElementById("tbMatKhau").innerText = "Mật khẩu phải chứa ít nhất 1 ký tự số, 1 ký tự in hoa và 1 ký tự đặc biệt";
    document.getElementById("tbMatKhau").style.display = "inline";
    return false;
  } else {
    document.getElementById("tbMatKhau").innerText = "";
    document.getElementById("tbMatKhau").style.display = "none";
    return true;
  }
}
function kiemTraNgayLam(ngayLam) {
  if (ngayLam === '') {
    document.getElementById("tbNgay").innerText = "Ngày làm không được để trống";
    document.getElementById("tbNgay").style.display = "inline";
    return false;
  } else {
    var datePattern = /^\d{2}\/\d{2}\/\d{4}$/;
    if (!datePattern.test(ngayLam)) {
      document.getElementById("tbNgay").innerText = "Định dạng ngày làm không hợp lệ";
      document.getElementById("tbNgay").style.display = "inline";
      return false;
    } else {
      document.getElementById("tbNgay").innerText = "";
      document.getElementById("tbNgay").style.display = "none";
      return true;
    }
  }
}
function kiemTraLuongCoBan(luong) {
  var minLuong = 1000000;
  var maxLuong = 20000000;

  if (luong === '') {
    document.getElementById("tbLuongCB").innerText = "Lương cơ bản không được để trống";
    document.getElementById("tbLuongCB").style.display = "inline";
    return false;
  } else if (luong < minLuong || luong > maxLuong) {
    document.getElementById("tbLuongCB").innerText = "Lương cơ bản phải là một số từ 1,000,000 đến 20,000,000";
    document.getElementById("tbLuongCB").style.display = "inline";
    return false;
  } else {
    document.getElementById("tbLuongCB").innerText = "";
    document.getElementById("tbLuongCB").style.display = "none";
    return true;
  }
}
function kiemTraChucVu(chucVu) {
  var chucVuHopLe = ["Sếp", "Trưởng phòng", "Nhân viên"];

  if (chucVu === '') {
    document.getElementById("tbChucVu").innerText = "Chức vụ không được để trống";
    document.getElementById("tbChucVu").style.display = "inline";
    return false;
  } else if (!chucVuHopLe.includes(chucVu)) {
    document.getElementById("tbChucVu").innerText = "Chức vụ không hợp lệ";
    document.getElementById("tbChucVu").style.display = "inline";
    return false;
  } else {
    document.getElementById("tbChucVu").innerText = "";
    document.getElementById("tbChucVu").style.display = "none";
    return true;
  }
}

function kiemTraSoGioLamTrongThang(gioLam ) {
  var minSoGioLam = 80;
  var maxSoGioLam = 200;

  if (gioLam  < minSoGioLam || gioLam  > maxSoGioLam) {
    document.getElementById("tbGiolam").innerText = "Số giờ làm trong tháng phải nằm trong khoảng từ 80 đến 200 giờ";
    document.getElementById("tbGiolam").style.display = "inline";
    return false;
  } else {
    document.getElementById("tbGiolam").innerText = "";
    document.getElementById("tbGiolam").style.display = "none";
    return true;
  }
}




